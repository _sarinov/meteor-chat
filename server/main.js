import { Meteor } from 'meteor/meteor';
import { ChatsCollection } from '/imports/api/chats';
import { MessagesCollection } from '/imports/api/messages';

async function insertChat({ name, avatar, chat_id }) {
  await ChatsCollection.insertAsync({ name, avatar, chat_id});
}

Meteor.startup(async () => {

  if (await ChatsCollection.find().countAsync() === 0) {
    await insertChat({
      chat_id: 1,
      name: 'Asyl',
      avatar: 'http://1.gravatar.com/avatar/7ec0cac01b6d505b2bbb2951a722e202?size=80',
    });

    await insertChat({
      chat_id: 2,
      name: 'Kot',
      avatar: 'http://1.gravatar.com/avatar/34735b367f6bf8d5d2f38cb3d20d5e36?size=80',
    });
  }

  Meteor.publish("chats", function () {
    return ChatsCollection.find();
  });

});

Meteor.methods({
  'message.send': function ({chat_id, message, from_id }) {
    MessagesCollection.insert({ chat_id, message, from_id })
    return MessagesCollection.find({ chat_id }).fetch()
  },
  'message.get': function ({ chat_id }) {
    const res =  MessagesCollection.find({ chat_id }).fetch()
    return res
  }
})
