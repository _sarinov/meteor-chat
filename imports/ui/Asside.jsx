import React, { useState } from 'react';
import { ChatsCollection } from '../api/chats';
import { useFind, useSubscribe } from 'meteor/react-meteor-data';
import { Chat } from './Chat';

export const Asside = () => {
  const isLoading = useSubscribe('chats');
  const chats = useFind(() => ChatsCollection.find());
  const [selectedUser, setSelectedUser] = useState(null)

  if(isLoading()) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <aside className="conv-list-view">
        <header className="conv-list-view__header">
          <div className="cf">
            <ul className="close-button-list">
              <li></li><li></li><li></li>
            </ul>
            <ul className="function-list">
              <li className="icon-lupe"></li>
            </ul>
          </div>
        </header>
        <ul className="conv-list">
          {chats.map(chat => (
            <li key={chat._id} onClick={() => setSelectedUser(chat)}>
            <div className="status">
              <i className="status__indicator--unread-message"></i>    
              <figure className="status__avatar">
                <img src={chat.avatar}/>
              </figure>
              <div className="meta">
                <div className="meta__name">{chat.name}</div>
              </div>
            </div>
          </li>    
          ))}
        </ul>
      </aside>
      <Chat selectedUser={selectedUser} />
      </>
  );
};
