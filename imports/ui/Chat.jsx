import React, { useEffect, useState} from 'react';

export const Chat = ({selectedUser}) => {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');

  
  useEffect(() => {
    if(selectedUser)
      Meteor.call('message.get', {chat_id: selectedUser.chat_id},(error, result) => {
      if (error) {
        console.log(error)
      } else {
        setMessages(result)
      }
    });
  }, [selectedUser]);

  if(!selectedUser){
    return <></>
  }

  const typing = (event) => {
    if (event.key === "Enter") {
      send()
    }
    setNewMessage(event.target.value)
  }
  const send = () => {
      Meteor.call('message.send', { chat_id: selectedUser.chat_id, message: newMessage, from_id: 3 }, (err, res) => {
        if (err) { return onError(err); }
        setMessages(res);
    })
  }
  return (
    <section className="chat-view">
    <header className="chat-view__header">
      <div className="cf">
        <div className="status">
          <i className="status__indicator--online"></i>  
          <div className="meta">
            <div className="meta__name">{selectedUser.name}</div>
          </div>
        </div>
        <ul className="function-list">
          <li className="icon-cloud"></li>
          <li className="icon-clock"></li>
          <li className="icon-dots"></li>
        </ul>
      </div>
    </header>
    <section className="message-view">
      {
        messages.map(ms=>(
          <>
          <div key={ms._id} className={ms.from_id == 3 ? 'message' :'message--send'}>
            <div className="message__bubble--send">
              {ms.message}
            </div>
            <figure className="message__avatar">
              <img src="http://1.gravatar.com/avatar/89b9501f0f9e3020aab173f9a5a47683?size=80" />
            </figure>
          </div>
          {ms.from_id == selectedUser._id ? <div className="cf"></div> : ''}
          </>
        ))
      }
    </section>
    <footer className="chat-view__input">
      <div className="input"><input onKeyUp={typing}/><span className="input__emoticon"></span></div>
      <div className="status">
        <figure className="status__avatar--small">
          <img src="http://1.gravatar.com/avatar/89b9501f0f9e3020aab173f9a5a47683?size=80" />
        </figure>
      </div>
    </footer>
  </section>
  );
};
