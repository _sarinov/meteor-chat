import React from 'react';
import { Asside } from './Asside';

export const App = () => (
  <div className="window">
    <Asside/>
  </div>
);
